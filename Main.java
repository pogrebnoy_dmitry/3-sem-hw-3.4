package org.sample;

import static java.lang.Math.log;

public class Main {
    public static int[] input;        /* input array  */
    public static int[] output;        /* output array */
    public static int LENGTH = 8;
    public static int NUMBER_OF_THREADS = LENGTH;
    public static int LOG_NUMBER_OF_THREADS = (int) (log(NUMBER_OF_THREADS) / log(2));
    public static int SECTION = LENGTH / NUMBER_OF_THREADS;
    public static Wall wall;
    private static int[][] upSweepTable;
    private static int[][] downSweepTable;

    public static void main(String[] args) throws InterruptedException {
        String input = "(()))(()";
        Main.input = StringToArray(input);

        upSweepTable = new int[LOG_NUMBER_OF_THREADS + 1][LENGTH];
        downSweepTable = new int[LOG_NUMBER_OF_THREADS + 1][LENGTH];
        output = new int[LENGTH + 1];
        wall = new Wall(LENGTH);

        for (int i = LENGTH - 1; i > 0; i--) {
            new PrefixScan(i, upSweepTable, downSweepTable).start();
        }

        PrefixScan last = new PrefixScan(0, upSweepTable, downSweepTable);
        last.start();
        last.join();


        for (int i = 0; i < LENGTH; i++) {
            if (output[i] < 0) {
                System.out.println("Wrong!");
                return;
            }
        }
        if (output[LENGTH] == 0) {
            System.out.println("Ok");
        } else {
            System.out.println("Wrong!");
        }

    }

    public static boolean singleThreadImpl(String inputString) {
        //For the obvious case
        if (inputString.length() % 2 != 0){
            return false;
        }

        int counter = 0;
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == '(') {
                counter++;
            }
            if (inputString.charAt(i) == ')') {
                if (counter > 0) {
                    counter--;
                } else {
                    return false;
                }
            }
        }
        return (counter == 0);
    }

    public static int[] StringToArray(String s) {
        String[] array = s.split("");
        int[] a = new int[LENGTH];

        for (int i = 0; i < LENGTH; i++) {
            if (array[i].equals("(")) {
                a[i] = 1;
            } else if (array[i].equals(")")) {
                a[i] = -1;
            }
        }
        return a;
    }
}