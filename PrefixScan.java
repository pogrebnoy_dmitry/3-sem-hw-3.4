package org.sample;

public class PrefixScan extends Thread {
    int idThread;
    int[][] upSweepTable;
    int[][] downSweepTable;

    public PrefixScan(int idThread, int[][] upSweepTable, int[][] downSweepTable) {
        this.idThread = idThread;
        this.upSweepTable = upSweepTable;
        this.downSweepTable = downSweepTable;
    }

    public void run() {
        int depth = Main.LOG_NUMBER_OF_THREADS;
        upSweepTable[depth][idThread] = Main.input[idThread];
        Main.wall.allSynchronized();
        prefix(depth);
        Main.output[idThread] = downSweepTable[depth][idThread];
        Main.wall.allSynchronized();
    }


    private void prefix(int depth) {
        if (depth == 0) {
            if (idThread == 0) {
                Main.output[Main.LENGTH] = upSweepTable[0][0];
            }
            Main.wall.allSynchronized();
            return;
        }

        if (idThread < 1 << (depth - 1)) {
            upSweepTable[depth - 1][idThread] = upSweepTable[depth][2 * idThread + 1] + upSweepTable[depth][2 * idThread];
        }
        Main.wall.allSynchronized();

        prefix(depth - 1);

        if (idThread < 1 << depth) {
            if (idThread == 1) {
                downSweepTable[depth][1] = downSweepTable[depth][0];
            } else if (idThread % 2 == 0) {
                downSweepTable[depth][idThread] = downSweepTable[depth - 1][idThread / 2];
            } else {
                downSweepTable[depth][idThread] = downSweepTable[depth - 1][(idThread - 1) / 2] + upSweepTable[depth][idThread - 1];
            }
        }
        Main.wall.allSynchronized();
    }
}
