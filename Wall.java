package org.sample;

public class Wall {
    private int maxThreadEnd;
    private int curThreadEnd;

    Wall(int n) {
        maxThreadEnd = n;
        curThreadEnd = 0;
    }

    synchronized void allSynchronized() {
        curThreadEnd++;
        if (curThreadEnd < maxThreadEnd) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            notifyAll();
            curThreadEnd = 0;
        }
    }
}
